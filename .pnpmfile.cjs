const { deps } = require('./getDep');


function readPackage(pkg, context) {
  
    
    for (const key in pkg.dependencies) {
        if (deps.has(key)) {
            pkg.dependencies[key] = 'workspace:*'
        }
    }

    return pkg
  }
  
  module.exports = {
    hooks: {
      readPackage
    }
  }