const { buildSync } = require('esbuild');
const { join } = require('path');
const { deps } = require('./getDep');

const aliasArr = [...deps.keys()];

const alias = {};
for (const name of aliasArr) {
    const srcPath = deps.get(name).srcPath;
    alias[`${name}`] = srcPath;
    alias[`${name}/lib`] = srcPath;
    alias[`${name}/es`] = srcPath;
}

buildSync({
    entryPoints: [
        './packages/ant-design/components/index.ts',
    ],
    sourcemap: 'linked',
    outdir: 'build',
    write: true,
    bundle: true,
    format: 'esm',
    target: ['es2022'],
    keepNames: true,
    platform: "browser",
    alias,
    external: [
        'react',
        'react-dom',
    ],
    loader: {
        '.tsx': 'tsx',
        '.ts': 'ts',
        '.jsx': 'jsx',
        '.js': 'jsx'
    },
    jsx: 'transform',
    jsxFragment: 'React.createElement',
    jsxFragment: 'React.Fragment'
});
