const { join } = require('path');
const { readdirSync } = require('fs');


function getDeps() {
    const files = readdirSync('./packages');

    const pkgs = files.map(file => {
        const dirPath = join(__dirname, 'packages', file);
        const srcPath = join(dirPath, 'src');
        const pkgPath = join(dirPath, 'package.json');
        const pkg = require(pkgPath);

        return {
            name: pkg.name,
            pkgPath,
            dirPath,
            srcPath
        }
    }).filter(({ name }) => name);


    const res = [...pkgs];
    const map = new Map();

    for (const v of res) {
        map.set(v.name, v);
    }
    return map;

}

module.exports = {
    deps: getDeps(),
}
