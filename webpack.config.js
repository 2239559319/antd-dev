const { join } = require('path');
const { deps } = require('./getDep');

const aliasArr = [...deps.keys()];

const alias = {};
for (const name of aliasArr) {
    const srcPath = deps.get(name).srcPath;
    alias[`${name}$`] = srcPath;
    alias[`${name}/lib`] = srcPath;
    alias[`${name}/es`] = srcPath;
}


/**
 * @type {import('webpack').Configuration}
 */
const config = {
    entry: {
        antd: './packages/ant-design/components/index.ts'
    },
    output: {
        path: join(__dirname, 'dist'),
        filename: 'antd.js',
        library: {
            name: 'antd',
            type: 'umd'
        }
    },
    devtool: 'source-map',
    mode: 'production',
    resolve: {
        alias,
        extensions: ['.tsx', '.ts', '.jsx', '.js']
    },
    module: {
        rules: [
            {
                test: /\.[jt]sx?$/,
                loader: 'ts-loader',
                options: {
                    transpileOnly: true,
                    compilerOptions: {
                        sourceMap: true
                    }
                }
            }
        ]
    },
    externals: {
        react: 'React',
        'react-dom': 'ReactDOM'
    }
};

module.exports = config;
